//
//  ItemCollectionViewCell.swift
//  IOS-PREMLEAPHENG-HW-004
//
//  Created by BTB_015 on 12/2/20.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        desc.textAlignment = .left
        
        self.desc.text = "Short Title";
        [self.desc, sizeToFit] as [Any];

    }

}

