//
//  ItemModel.swift
//  IOS-PREMLEAPHENG-HW-004
//
//  Created by BTB_015 on 12/2/20.
//

import Foundation

struct Item {
    var title: String
    var description: String
}
