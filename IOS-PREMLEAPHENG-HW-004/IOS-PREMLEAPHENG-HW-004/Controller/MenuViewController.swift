//
//  MenuViewController.swift
//  IOS-PREMLEAPHENG-HW-004
//
//  Created by BTB_015 on 12/2/20.
//

import UIKit

class MenuViewController: UIViewController {

    
    @IBOutlet weak var colletionView: UICollectionView!
    @IBOutlet weak var textANote: UIBarButtonItem!
    
    var itemObj = [Item]()
    var objDelegate: DataSender?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        itemObj.append(Item(title: "Monday", description: "consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex"))
        itemObj.append(Item(title: "Tusday", description: "consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex"))
        itemObj.append(Item(title: "Wednesday", description: "consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex"))
        itemObj.append(Item(title: "Thursday", description: "consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex"))
        itemObj.append(Item(title: "Friday", description: "consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex"))
        itemObj.append(Item(title: "Sarturday", description: "consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex"))
        itemObj.append(Item(title: "Sunday", description: "consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex"))
        

        colletionView.register(UINib(nibName: "ItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "itemCell")
        colletionView.delegate = self
        colletionView.dataSource = self
        colletionView.showsVerticalScrollIndicator = false
        
        
    }
    @IBAction func changLanguage(_ sender: Any) {

        let alert = UIAlertController(title:.none , message: .none , preferredStyle: UIAlertController.Style.actionSheet)

        
                    alert.addAction(UIAlertAction(title: "Khmer", style: .default, handler: { _ in
                    let path = Bundle.main.path(forResource: "km-KH", ofType: "lproj")
                    let bundle = Bundle.init(path: path!)! as Bundle
                    self.title = bundle.localizedString(forKey: "noted", value: nil, table: nil)
                    self.textANote.title = bundle.localizedString(forKey: "textANote", value: nil, table: nil)
                    
                    
                }))
                
//        en.lproj
                alert.addAction(UIAlertAction(title: "English", style: .default,
                handler: {(_: UIAlertAction!) in
                    
                    let path = Bundle.main.path(forResource: "en", ofType: "lproj")
                    let bundle = Bundle.init(path: path!)! as Bundle
                    self.title = bundle.localizedString(forKey: "noted", value: nil, table: nil)
                    self.textANote.title = bundle.localizedString(forKey: "textANote", value: nil, table: nil)
                    
                    
                }))
        
        
                self.present(alert, animated: true, completion: nil)

           
    }
}

extension MenuViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemObj.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCell", for: indexPath) as! ItemCollectionViewCell
        
        cell.title.text = itemObj[indexPath.row].title
        cell.desc.text = itemObj[indexPath.row].description
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "editView") as! EditViewController
        
        self.objDelegate = secondViewController
        objDelegate?.itemData(title: itemObj[indexPath.row].title, desc: itemObj[indexPath.row].description)

        self.navigationController?.pushViewController(secondViewController, animated: true)
        
        
        }
             
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 170, height: 170)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
      return 24
   }
    
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 25, left: 25, bottom: 0, right: 25)
   }
}


protocol DataSender {
    func itemData(title: String, desc: String)
}
