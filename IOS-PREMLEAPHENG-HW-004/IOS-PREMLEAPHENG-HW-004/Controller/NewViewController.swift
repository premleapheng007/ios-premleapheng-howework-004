//
//  NewViewController.swift
//   
//
//  Created by BTB_015 on 12/3/20.
//

import UIKit

class NewViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textField.borderStyle = .none;
      
    }
}
