//
//  EditViewController.swift
//   
//
//  Created by JONNY on 9/12/1399 AP.
//

import UIKit

class EditViewController: UIViewController, DataSender {
    

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var titleLabel: UITextField!
    
    var textTitle: String?
    var textViewDesc: String?
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.borderStyle = .none;
        titleLabel.text = textTitle
        textView.text = textViewDesc
    }
    
    func itemData(title: String, desc: String) {
        
        textTitle = title
        textViewDesc = desc
        
    }

}
